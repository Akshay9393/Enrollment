﻿// Decompiled with JetBrains decompiler
// Type: EnrollmentVerificationReport.AppLogger
// Assembly: EnrollmentVerificationReport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EE743BFF-1357-4436-BBBD-B284FB97316A
// Assembly location: C:\Users\akshay.bhaskar\Documents\My Received Files\Release_01242019\EnrollmentVerificationReport.exe

using System;
using System.IO;

namespace EnrollmentVerificationReport
{
  internal static class AppLogger
  {
    private static StreamWriter objFile;

    private static bool CreateLogFile()
    {
      try
      {
        if (!Directory.Exists(Directory.GetCurrentDirectory().ToString() + "\\Logs"))
          Directory.CreateDirectory(Directory.GetCurrentDirectory().ToString() + "\\Logs");
        string str1 = Directory.GetCurrentDirectory().ToString();
        DateTime now = DateTime.Now;
        string str2 = now.ToString("yyyyMMdd");
        if (!File.Exists(str1 + "\\Logs\\EnrollmentReport_" + str2 + ".Txt"))
        {
          string str3 = Directory.GetCurrentDirectory().ToString();
          now = DateTime.Now;
          string str4 = now.ToString("yyyyMMdd");
          File.Create(str3 + "\\Logs\\EnrollmentReport_" + str4 + ".Txt").Close();
        }
        string str5 = Directory.GetCurrentDirectory().ToString();
        now = DateTime.Now;
        string str6 = now.ToString("yyyyMMdd");
        AppLogger.objFile = new StreamWriter(str5 + "\\Logs\\EnrollmentReport_" + str6 + ".Txt", true);
        return true;
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message.ToString());
        Console.Read();
        return false;
      }
    }

    public static void MakeEntry(string strLog)
    {
      try
      {
        AppLogger.CreateLogFile();
        if (AppLogger.objFile == null)
          return;
        AppLogger.objFile.WriteLine(strLog);
        AppLogger.objFile.Close();
        AppLogger.objFile.Dispose();
        AppLogger.objFile = (StreamWriter) null;
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message.ToString());
        Console.Read();
      }
      finally
      {
        GC.Collect();
      }
    }

    public static void MakeEntry(string strLog, string fileName)
    {
      try
      {
        AppLogger.CreateLogFile(fileName);
        if (AppLogger.objFile == null)
          return;
        AppLogger.objFile.WriteLine(strLog);
        AppLogger.objFile.Close();
        AppLogger.objFile.Dispose();
        AppLogger.objFile = (StreamWriter) null;
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message.ToString());
        Console.Read();
      }
      finally
      {
        GC.Collect();
      }
    }

    private static bool CreateLogFile(string fileName)
    {
      try
      {
        if (!Directory.Exists(Directory.GetCurrentDirectory().ToString() + "\\Reports"))
          Directory.CreateDirectory(Directory.GetCurrentDirectory().ToString() + "\\Reports");
        if (!File.Exists(Directory.GetCurrentDirectory().ToString() + "\\Reports\\" + fileName))
          File.Create(Directory.GetCurrentDirectory().ToString() + "\\Reports\\" + fileName).Close();
        AppLogger.objFile = new StreamWriter(Directory.GetCurrentDirectory().ToString() + "\\Reports\\" + fileName, true);
        return true;
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message.ToString());
        Console.Read();
        return false;
      }
    }
  }
}
