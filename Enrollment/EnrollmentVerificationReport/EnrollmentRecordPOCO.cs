﻿// Decompiled with JetBrains decompiler
// Type: EnrollmentVerificationReport.EnrollmentRecordPOCO
// Assembly: EnrollmentVerificationReport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EE743BFF-1357-4436-BBBD-B284FB97316A
// Assembly location: C:\Users\akshay.bhaskar\Documents\My Received Files\Release_01242019\EnrollmentVerificationReport.exe

using System;

namespace EnrollmentVerificationReport
{
  internal class EnrollmentRecordPOCO
  {
    public Decimal Session_Id { get; set; }

    public string Ucid { get; set; }

    public string Timestamp { get; set; }

    public string Activity { get; set; }

    public string FinalStatus { get; set; }

    public override string ToString()
    {
      return this.Session_Id.ToString() + "     " + this.Ucid + "    " + this.Timestamp + "   " + this.Activity + "    " + this.FinalStatus;
    }
  }
}
