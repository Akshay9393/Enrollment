﻿// Decompiled with JetBrains decompiler
// Type: EnrollmentVerificationReport.CreateReportDAO
// Assembly: EnrollmentVerificationReport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EE743BFF-1357-4436-BBBD-B284FB97316A
// Assembly location: C:\Users\akshay.bhaskar\Documents\My Received Files\Release_01242019\EnrollmentVerificationReport.exe

using Oracle.DataAccess.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace EnrollmentVerificationReport
{
  internal class CreateReportDAO
  {
    private static string enrollmentQuery = ConfigurationSettings.AppSettings["EnrollmentQuery"];
    private static string verificationQuery = ConfigurationSettings.AppSettings["VerficationQuery"];
    private static string customStartDate = ConfigurationSettings.AppSettings["CustomStartDate"];
    private static string customEndDate = ConfigurationSettings.AppSettings["CustomEndDate"];

    public List<EnrollmentRecordPOCO> GetEnrollMentReport()
    {
      OracleDataReader oracleDataReader = (OracleDataReader) null;
      string connectionString = AppUtils.getConnectionString();
      if (CreateReportDAO.customStartDate != "" && CreateReportDAO.customEndDate != "")
      {
        CreateReportDAO.enrollmentQuery = CreateReportDAO.enrollmentQuery.Replace("?START_DATE?", "'" + CreateReportDAO.customStartDate + "'");
        CreateReportDAO.enrollmentQuery = CreateReportDAO.enrollmentQuery.Replace("?END_DATE?", "'" + CreateReportDAO.customEndDate + "'");
      }
      else
      {
        DateTime now = DateTime.Now;
        DateTime date = DateTime.Today.AddDays(-1.0);
        CreateReportDAO.enrollmentQuery = CreateReportDAO.enrollmentQuery.Replace("?START_DATE?", "'" + AppUtils.GetDateZeroTime(now).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'");
        CreateReportDAO.enrollmentQuery = CreateReportDAO.enrollmentQuery.Replace("?END_DATE?", "'" + AppUtils.GetDateZeroTime(date).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'");
      }
      Console.WriteLine(CreateReportDAO.enrollmentQuery);
      OracleConnection conn = new OracleConnection(connectionString);
      List<EnrollmentRecordPOCO> enrollmentRecordPocoList = new List<EnrollmentRecordPOCO>();
      try
      {
        conn.Open();
        ArrayList arrayList = new ArrayList();
        oracleDataReader = new OracleCommand(CreateReportDAO.enrollmentQuery, conn).ExecuteReader();
        while (oracleDataReader.Read())
        {
          EnrollmentRecordPOCO enrollmentRecordPoco = new EnrollmentRecordPOCO();
          object[] objArray = new object[oracleDataReader.FieldCount];
          enrollmentRecordPoco.Session_Id = (Decimal) oracleDataReader["session_id"];
          enrollmentRecordPoco.Ucid = (string) oracleDataReader["ucid"];
          enrollmentRecordPoco.Timestamp = (string) oracleDataReader["timestamp"];
          enrollmentRecordPoco.Activity = (string) oracleDataReader["activity"];
          enrollmentRecordPoco.FinalStatus = (string) oracleDataReader["final_status"];
          enrollmentRecordPocoList.Add(enrollmentRecordPoco);
        }
      }
      catch (OracleException ex)
      {
        Console.WriteLine("DataBase Error Had Occured Please check Log File For Details");
        AppLogger.MakeEntry(ex.Message);
      }
      finally
      {
        oracleDataReader?.Close();
        conn.Close();
      }
      return enrollmentRecordPocoList;
    }

    public List<VerificationRecordsPOCO> GetVerficationReport()
    {
      OracleDataReader oracleDataReader = (OracleDataReader) null;
      string connectionString = AppUtils.getConnectionString();
      if (CreateReportDAO.customStartDate != "" && CreateReportDAO.customEndDate != "")
      {
        CreateReportDAO.verificationQuery = CreateReportDAO.verificationQuery.Replace("?START_DATE?", "'" + CreateReportDAO.customStartDate + "'");
        CreateReportDAO.verificationQuery = CreateReportDAO.verificationQuery.Replace("?END_DATE?", "'" + CreateReportDAO.customEndDate + "'");
      }
      else
      {
        DateTime now = DateTime.Now;
        DateTime date = DateTime.Today.AddDays(-1.0);
        CreateReportDAO.verificationQuery = CreateReportDAO.verificationQuery.Replace("?START_DATE?", "'" + AppUtils.GetDateZeroTime(now).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'");
        CreateReportDAO.verificationQuery = CreateReportDAO.verificationQuery.Replace("?END_DATE?", "'" + AppUtils.GetDateZeroTime(date).ToString("dd-MMM-yyyy hh:mm:ss tt") + "'");
      }
      Console.WriteLine(CreateReportDAO.verificationQuery);
      OracleConnection conn = new OracleConnection(connectionString);
      List<VerificationRecordsPOCO> verificationRecordsPocoList = new List<VerificationRecordsPOCO>();
      try
      {
        conn.Open();
        oracleDataReader = new OracleCommand(CreateReportDAO.verificationQuery, conn).ExecuteReader();
        while (oracleDataReader.Read())
        {
          VerificationRecordsPOCO verificationRecordsPoco = new VerificationRecordsPOCO();
          object[] objArray = new object[oracleDataReader.FieldCount];
          verificationRecordsPoco.Session_Id = (Decimal) oracleDataReader["session_id"];
          verificationRecordsPoco.Ucid = (string) oracleDataReader["ucid"];
          verificationRecordsPoco.Timestamp = (string) oracleDataReader["timestamp"];
          verificationRecordsPoco.Activity = (string) oracleDataReader["activity"];
          verificationRecordsPoco.FinalStatus = (string) oracleDataReader["final_status"];
          verificationRecordsPocoList.Add(verificationRecordsPoco);
        }
      }
      catch (OracleException ex)
      {
        Console.WriteLine("DataBase Error Had Occured Please check Log File For Details");
        AppLogger.MakeEntry(ex.Message);
      }
      finally
      {
        oracleDataReader?.Close();
        conn.Close();
      }
      return verificationRecordsPocoList;
    }
  }
}
