﻿// Decompiled with JetBrains decompiler
// Type: EnrollmentVerificationReport.AppUtils
// Assembly: EnrollmentVerificationReport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EE743BFF-1357-4436-BBBD-B284FB97316A
// Assembly location: C:\Users\akshay.bhaskar\Documents\My Received Files\Release_01242019\EnrollmentVerificationReport.exe

using Oracle.DataAccess.Client;
using System;
using System.Configuration;
using System.Text;

namespace EnrollmentVerificationReport
{
  internal class AppUtils
  {
    private static string customStartDate = ConfigurationSettings.AppSettings["CustomStartDate"];
    private static string customEndDate = ConfigurationSettings.AppSettings["CustomEndDate"];
    private static string connection = ConfigurationSettings.AppSettings[nameof (connection)];

    public static string getConnectionString()
    {
      string appSetting = ConfigurationSettings.AppSettings["DbPassword"];
      return AppUtils.connection + "Password=" + AppUtils.Base64Decode(appSetting) + ";";
    }

    private static string GetString(byte[] bytes)
    {
      char[] chArray = new char[bytes.Length / 2];
      Buffer.BlockCopy((Array) bytes, 0, (Array) chArray, 0, bytes.Length);
      return new string(chArray);
    }

    public static string getHaResourceConnectionDetails()
    {
      OracleConnection conn = new OracleConnection(AppUtils.getConnectionString());
      string empty = string.Empty;
      try
      {
        conn.Open();
        OracleDataReader oracleDataReader = new OracleCommand("select Details from nvb_ha_resources where INTERNAL_NAME='DATABASE1'", conn).ExecuteReader();
        while (oracleDataReader.Read())
          empty = oracleDataReader[0].ToString();
      }
      catch (Exception ex)
      {
        AppLogger.MakeEntry(ex.Message);
      }
      finally
      {
        conn.Close();
        conn.Dispose();
      }
      return empty;
    }

    public static string Base64Decode(string base64EncodedData)
    {
      return Encoding.UTF8.GetString(Convert.FromBase64String(base64EncodedData));
    }

    public static DateTime GetDateZeroTime(DateTime date)
    {
      return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
    }

    public static string GetYesterdayDateTime(string prefix)
    {
      DateTime date = DateTime.Today.AddDays(-1.0);
      if (AppUtils.customStartDate != "" && AppUtils.customEndDate != "")
        date = DateTime.Parse(AppUtils.customStartDate);
      return prefix + "-" + AppUtils.GetDateZeroTime(date).ToString("yyyymmdd");
    }
  }
}
