﻿// Decompiled with JetBrains decompiler
// Type: EnrollmentVerificationReport.Program
// Assembly: EnrollmentVerificationReport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EE743BFF-1357-4436-BBBD-B284FB97316A
// Assembly location: C:\Users\akshay.bhaskar\Documents\My Received Files\Release_01242019\EnrollmentVerificationReport.exe

using System.Configuration;

namespace EnrollmentVerificationReport
{
  internal class Program
  {
    private string connection = ConfigurationSettings.AppSettings[nameof (connection)];

    private static void Main(string[] args)
    {
      Program.getCreateReportBO().createReport();
    }

    private static CreateReportBO getCreateReportBO()
    {
      return new CreateReportBO();
    }
  }
}
