﻿// Decompiled with JetBrains decompiler
// Type: EnrollmentVerificationReport.CreateReportBO
// Assembly: EnrollmentVerificationReport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: EE743BFF-1357-4436-BBBD-B284FB97316A
// Assembly location: C:\Users\akshay.bhaskar\Documents\My Received Files\Release_01242019\EnrollmentVerificationReport.exe

using System;
using System.Collections.Generic;
using System.Text;

namespace EnrollmentVerificationReport
{
  internal class CreateReportBO
  {
    public bool createReport()
    {
      this.writeEnrollReport(this.getCreateReportDAO().GetEnrollMentReport());
      this.writeVerificationReport(this.getCreateReportDAO().GetVerficationReport());
      return false;
    }

    public bool writeEnrollReport(List<EnrollmentRecordPOCO> listEnroll)
    {
      Dictionary<string, EnrollmentRecordPOCO> dictionary = new Dictionary<string, EnrollmentRecordPOCO>();
      StringBuilder stringBuilder = new StringBuilder("SessionId Ucid Timestamp\tActivityFinal\tStatus");
      foreach (EnrollmentRecordPOCO enrollmentRecordPoco in listEnroll)
      {
        string key = enrollmentRecordPoco.Session_Id.ToString();
        if (enrollmentRecordPoco.Ucid != null || enrollmentRecordPoco.Ucid != "")
          key = enrollmentRecordPoco.Session_Id.ToString() + enrollmentRecordPoco.Ucid;
        if (dictionary.ContainsKey(key))
          dictionary[key] = enrollmentRecordPoco;
        else
          dictionary.Add(key, enrollmentRecordPoco);
      }
      foreach (KeyValuePair<string, EnrollmentRecordPOCO> keyValuePair in dictionary)
      {
        EnrollmentRecordPOCO enrollmentRecordPoco = keyValuePair.Value;
        stringBuilder.Append(Environment.NewLine);
        stringBuilder.Append(enrollmentRecordPoco.ToString());
      }
      AppLogger.MakeEntry(stringBuilder.ToString(), AppUtils.GetYesterdayDateTime("TransactionSummary"));
      return false;
    }

    public bool writeVerificationReport(List<VerificationRecordsPOCO> listVerify)
    {
      Dictionary<string, VerificationRecordsPOCO> dictionary = new Dictionary<string, VerificationRecordsPOCO>();
      StringBuilder stringBuilder = new StringBuilder();
      foreach (VerificationRecordsPOCO verificationRecordsPoco in listVerify)
      {
        Decimal sessionId = verificationRecordsPoco.Session_Id;
        string key = sessionId.ToString();
        if (verificationRecordsPoco.Ucid != null || verificationRecordsPoco.Ucid != "")
        {
          sessionId = verificationRecordsPoco.Session_Id;
          key = sessionId.ToString() + verificationRecordsPoco.Ucid;
        }
        if (dictionary.ContainsKey(key))
          dictionary[key] = verificationRecordsPoco;
        else
          dictionary.Add(key, verificationRecordsPoco);
      }
      foreach (KeyValuePair<string, VerificationRecordsPOCO> keyValuePair in dictionary)
      {
        VerificationRecordsPOCO verificationRecordsPoco = keyValuePair.Value;
        stringBuilder.Append(verificationRecordsPoco.ToString());
        stringBuilder.Append(Environment.NewLine);
      }
      AppLogger.MakeEntry(stringBuilder.ToString(), AppUtils.GetYesterdayDateTime("TransactionSummary"));
      return false;
    }

    public CreateReportDAO getCreateReportDAO()
    {
      return new CreateReportDAO();
    }
  }
}
